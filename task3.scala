case class Cource(name: String, price: Int, tax: Int);

val pricesRDD = sc.textFile("/tests/prices.txt");

val pricesDF = pricesRDD.map(line=>new Cource(line.split(",")(0), line.split(",")(1).toInt, line.split(",")(2).toInt)).toDF();

val newPricesDF = pricesDF.withColumn("newPrice", pricesDF("price") + pricesDF("price")/100*pricesDF("tax"));

val resultRDD = newPricesDF.rdd.map(row=>row(0)+","+row(1)+","+row(2)+","+row(3));

resultRDD.repartition(1).saveAsTextFile("/tests/resultObject.txt");