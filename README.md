# Spark tasks for Hortonworks Spark certification preparation #

Okay, I signed myself for the [https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/](Hortonworks Spark Certification test)
to the beginning of 2018 September...There is no going back!!

## Task 1. Delayed flights ##

During the Hortonworks training I had last year we had been given the following bonus task:

You're given a **big data of flights** in the following example **structure (flight, delay, airlines)**:

    [root@sandbox ~]# cat flights.txt
    USA-CAN, 25, Flights, s.r.o
    CAN-ARG, 10, SuperWings
    GBR-OPQ, 16, SmartWings
    PHI-JPN, 9, SuperPlanes, USA
    AUS-JPN, 18, United Airlines
    [root@sandbox ~]#

**Task**: From the flights having the delay over 15 minutes find the flight with the shortes delay.
Data separator "," can also be used as part of the airlines name! Like "Flights, s.r.o". Here "," isn't a separator.

**Solution**:

    case class Flight(path: String, delay: Int, airlines: String);

    def getFlight(line: String): Flight={
        val arr = line.split(",");
        var path = "";
        var delay = 0;
        var airlines = "";
        for (i<-0 until arr.length) {
            if (i == 0) {
                path = arr(i);
            } else if (i == 1) {
                delay = arr(i).trim().toInt;
            } else {
                if (airlines == "") {
                    airlines = arr(i);
                } else {
                    airlines = airlines + "," + arr(i);
                }
            }
        }
    
        return new Flight(path, delay, airlines);
    }

    var fileRDD = sc.textFile("/tests/flights.txt");
    val flightDF = fileRDD.map(line=>getFlight(line)).toDF();

    val delayMin15RDD = sc.parallelize(flightDF.filter("delay > 15").orderBy(flightDF("delay")).take(1)).map(row=>row(0)+","+row(1)+","+row(2));

    delayMin15RDD.coalesce(1).saveAsTextFile("/tests/delays");

running the code in Zepellin should give the following file result:

    [root@sandbox ~]# hdfs dfs -ls /tests/delays/
    Found 2 items
    -rw-r--r--   1 zeppelin hdfs          0 2018-07-27 22:05 /tests/delays/_SUCCESS
    -rw-r--r--   1 zeppelin hdfs         23 2018-07-27 22:05 /tests/delays/part-00000
    [root@sandbox ~]# hdfs dfs -cat /tests/delays/part-00000
    GBR-OPQ,16, SmartWings

## Task 2. RDD filtering ##

At HDP Spark certification we can also expect following similar task...At least our hortonworks trainer mentioned that...:-)

**Task**: You're given a big data text file. Get new RDD from that with only **rows containing word "spark" more than once**...

Input file testfile.txt:

    [root@sandbox ~]# cat testfile.txt
    hello I learn spark because spark
    is the best
    but because of spark
    I can be more valueable
    [root@sandbox ~]#

**Solution**:

    val fileRDD = sc.textFile("/tests/testfile.txt");

    def wordCounter(line: String, word: String):Int={
        val arr = line.split(" ");
        var count:Int = 0;
        for (i <- 0 until arr.length) {
            if (arr(i).contains(word)) {
                count = count + 1;
            }
        }
    
        return count;
    }

    val filteredRDD = fileRDD.map(line=>(line, wordCounter(line, "spark"))).filter(pair=>pair._2 > 1);
    filteredRDD.collect();

Output should be (launched from Zepellin):

    fileRDD: org.apache.spark.rdd.RDD[String] = /tests/testfile.txt MapPartitionsRDD[84] at textFile at <console>:29
    wordCounter: (line: String, word: String)Int
    filteredRDD: org.apache.spark.rdd.RDD[(String, Int)] = MapPartitionsRDD[86] at filter at <console>:33
    res12: Array[(String, Int)] = Array((hello I learn spark because spark,2))

**Task 3. New price based on tax**

I found this task here. 

https://www.youtube.com/watch?v=DEGoESNnkm0&t=85s

**Task**:

You're given a big data of courses in the following sample structure (coursename, price, tax):

    [root@sandbox ~]# cat prices.txt
    spark,1067,20
    java,2000,15
    python,3000,30
    c++,400,30
    [root@sandbox ~]#

Task is to add new column to every row with computed final price based on: newPrice = price + price/100*tax.

**Solution:**

    case class Cource(name: String, price: Int, tax: Int);

    val pricesRDD = sc.textFile("/tests/prices.txt");

    val pricesDF = pricesRDD.map(line=>new Cource(line.split(",")(0), line.split(",")(1).toInt, line.split(",")(2).toInt)).toDF();

    val newPricesDF = pricesDF.withColumn("newPrice", pricesDF("price") + pricesDF("price")/100*pricesDF("tax"));

    val resultRDD = newPricesDF.rdd.map(row=>row(0)+","+row(1)+","+row(2)+","+row(3));

    resultRDD.repartition(1).saveAsTextFile("/tests/resultObject.txt");

Output should be:

    [root@sandbox ~]# hdfs dfs -cat /tests/resultObject.txt/part-00000
    spark,1067,20,1280.4
    java,2000,15,2300.0
    python,3000,30,3900.0
    c++,400,30,520.0
    [root@sandbox ~]#

I hope you liked my Spark training...:)

regards

Tomas












