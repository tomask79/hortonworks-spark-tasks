val fileRDD = sc.textFile("/tests/testfile.txt");

def wordCounter(line: String, word: String):Int={
    val arr = line.split(" ");
    var count:Int = 0;
    for (i <- 0 until arr.length) {
        if (arr(i).contains(word)) {
            count = count + 1;
        }
    }

    return count;
}

val filteredRDD = fileRDD.map(line=>(line, wordCounter(line, "spark"))).filter(pair=>pair._2 > 1);
filteredRDD.collect();