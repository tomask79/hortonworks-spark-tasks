case class Flight(path: String, delay: Int, airlines: String);

def getFlight(line: String): Flight={
    val arr = line.split(",");
    var path = "";
    var delay = 0;
    var airlines = "";
    for (i<-0 until arr.length) {
        if (i == 0) {
            path = arr(i);
        } else if (i == 1) {
            delay = arr(i).trim().toInt;
        } else {
            if (airlines == "") {
                airlines = arr(i);
            } else {
                airlines = airlines + "," + arr(i);
            }
        }
    }
    
    return new Flight(path, delay, airlines);
}

var fileRDD = sc.textFile("/tests/flights.txt");
val flightDF = fileRDD.map(line=>getFlight(line)).toDF();

val delayMin15RDD = sc.parallelize(flightDF.filter("delay > 15").orderBy(flightDF("delay")).take(1)).map(row=>row(0)+","+row(1)+","+row(2));

delayMin15RDD.coalesce(1).saveAsTextFile("/tests/delays");